import org.objectweb.asm.ClassReader;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.*;
import org.objectweb.asm.util.CheckClassAdapter;
import org.objectweb.asm.util.Printer;
import org.objectweb.asm.util.Textifier;
import org.objectweb.asm.util.TraceMethodVisitor;

import java.io.*;
import java.lang.reflect.Field;

public class ASMUtil {
    static public String[] opcodeNames = new String[256];
    static {
        boolean bytecodeStarts = false;
        try {
            Class cls = Class.forName("org.objectweb.asm.Opcodes");

            Field fieldlist[] = cls.getDeclaredFields();
            for (int i = 0; i < fieldlist.length; i++) {
                Field fld = fieldlist[i];
                if (fld.getName().equals("NOP")) {
                    bytecodeStarts = true;
                }

                if (bytecodeStarts) {
                    opcodeNames[fld.getInt(null)] = fld.getName();
                }
            }
        } catch (Throwable e) {
            System.err.println(e);
        }

    }

    public static ClassReader getClassReaderFromClassName(String classname) {
        InputStream in = ASMUtil.class.getResourceAsStream(classname.replace('.', '/')+".class");

        ClassReader cr = null;
        try {
            cr = new ClassReader(in);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("class: " + classname);
            System.exit(1);
        }
        assert cr != null;
        return cr;
    }

    /*
    Return 0 if verification succeeds.
     */
    /*
    public static int verifyClass(byte[] bytes) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        try {
            CheckClassAdapter.verify(new ClassReader(bytes), false, pw);
        }
        catch (Throwable e) {
            if (Options.v().printErrors) {
                System.out.println("got verify error:");
                e.printStackTrace();
            }

            return 1;
        }

        if (sw.toString().length() == 0) {
            return 0;
        }
        else {
            if (Options.v().printErrors) {
                System.out.println(sw.toString());
            }

            return 1;
        }

    }
    */

    public static void writeBytes(String filename, byte[] data) {
        try {
            System.out.println("write bytes to " + filename);
            JavaUtil.createDirIfNotExist(filename);
            FileOutputStream out=new FileOutputStream(filename);
            out.write(data);
            out.close();
        }
        catch (Exception e) {
            System.out.println("Write to file failed: " + filename);
            e.printStackTrace();
        }
    }

    //public static byte[] JSRInliner()

    public static String getMethodSig(MethodNode m) {
        return m.name + m.desc;
    }

    public static String getMethodSig(String classname, MethodNode m) {
        return classname + "." + m.name + m.desc;
    }

    public static void printInsns(MethodNode m) {
        System.out.println("Instructions for method " + getMethodSig(m));
        for (AbstractInsnNode insn: m.instructions.toArray()) {
            System.out.println("\t" + ASMUtil.insnToString(insn));
        }
    }

    public static String insnToString(AbstractInsnNode insn){
        insn.accept(mp);
        StringWriter sw = new StringWriter();
        printer.print(new PrintWriter(sw));
        printer.getText().clear();
        return sw.toString();
    }

    private static Printer printer = new Textifier();
    private static TraceMethodVisitor mp = new TraceMethodVisitor(printer);

    public static boolean containsMemAccess(AbstractInsnNode node) {
        if (node instanceof InsnNode) {
            int opcode = node.getOpcode();
            switch (opcode) {
                case Opcodes.IASTORE:
                case Opcodes.FASTORE:
                case Opcodes.AASTORE:
                case Opcodes.BASTORE:
                case Opcodes.CASTORE:
                case Opcodes.SASTORE:
                case Opcodes.DASTORE:
                case Opcodes.LASTORE:
                    return true;
                case Opcodes.IALOAD:
                case Opcodes.FALOAD:
                case Opcodes.AALOAD:
                case Opcodes.BALOAD:
                case Opcodes.CALOAD:
                case Opcodes.SALOAD:
                case Opcodes.DALOAD:
                case Opcodes.LALOAD:
                    return true;
            }
        }

        if (node instanceof FieldInsnNode) {
            return true;
        }

        return false;
    }

    public static boolean isAbstract(MethodNode methodNode) {
        return (methodNode.access & Opcodes.ACC_ABSTRACT) != 0;
    }

    public static boolean isStatic(MethodNode methodNode) {
        return (methodNode.access & Opcodes.ACC_STATIC) != 0;
    }

    public static boolean isStatic(MethodInsnNode methodNode) {
        return methodNode.getOpcode() == Opcodes.INVOKESTATIC;
    }
    
    public static String toString(VarInsnNode node) {
        return opcodeToString(node.getOpcode()) + " " + node.var;
    }
    
    public static String toString(MethodInsnNode node) {
        return opcodeToString(node.getOpcode()) + " " + node.owner + "." + node.name + " " + node.desc;
    }
    
    public static String toString(FieldInsnNode node) {
        return opcodeToString(node.getOpcode()) + " " + node.owner + "." + node.name + " " + node.desc;
    }
    
    public static String toString(TypeInsnNode node) {
        return opcodeToString(node.getOpcode()) + " " + node.desc;
    }
    
    public static String toString(LabelNode node) {
        return "LABEL";
    }
    
    public static String opcodeToString(int opcode) {
        return opcodeNames[opcode];
    }


}

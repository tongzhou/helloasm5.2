import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import static java.lang.System.err;
import static java.lang.System.out;

public class JavaUtil {
    static public File createDirIfNotExist(String filename) {
        File f = new File(FilenameUtils.getPath(filename));
        if (!f.exists()) {
            boolean success = f.mkdirs();
            if (!success) {
                err.println("Create dir failed: " + FilenameUtils.getPath(filename));
            }
        }
        return f;
    }

    static public void deleteSootOutputDir() {
        File oldfile = new File("sootOutput");
        if (oldfile.exists()) {
            try {
                FileUtils.deleteDirectory(oldfile);
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        return;
    }

    /**
     * Rename directory from `old` to `new` (delete `new` if exists)
     * @param oldname
     * @param newname
     */
    static public int renameDir(String oldname, String newname) {
        File newfile = new File(newname);
        File oldfile = new File(oldname);
        if (newfile.exists()) {
            try {
                FileUtils.deleteDirectory(newfile);
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        newfile.mkdirs();

        if (oldfile.renameTo(newfile)){
            System.out.println("Rename succesful");
            return 0;
        } else {
            System.out.println("Rename failed");
            return 1;
        }
    }

    static public void printObjectCounterFields(Object o) {
        Class<?> c = o.getClass();
        try {
            for (Field field : c.getDeclaredFields()) {
                if (!field.getName().endsWith("Count")) {
                    continue;
                }

                out.println(
                        field.getName() + ": " +  field.get(o).toString()
                );
            }
        }
        catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public static List<String> getClassesFromDir(String dirName) {
        return getClassesFromDirHelper(new File(dirName), dirName);
    }

    public static List<String> getClassesFromDirHelper(File dir, String topDirName) {
        ArrayList<String> classes = new ArrayList<>();
        File[] listOfFiles = dir.listFiles();
        if (listOfFiles == null) {
            err.println("No class found in dir: " + dir);
        }

        for (int i = 0; i < listOfFiles.length; i++) {
            File f = listOfFiles[i];
            if (f.isFile()) {
                if (f.getName().endsWith(".class")) {
                    String cls = f.getPath().substring(topDirName.length());
                    cls = cls.replace('/', '.').replace(".class", "");
                    if (cls.charAt(0) == '.') {
                        cls = cls.substring(1);
                    }
                    classes.add(cls);
                }
            }
            else if (f.isDirectory()) {
                classes.addAll(getClassesFromDirHelper(f, topDirName));
            }
        }
        return classes;
    }
}

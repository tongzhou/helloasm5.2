import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.*;
import org.objectweb.asm.util.Textifier;

import static java.lang.System.out;
import static org.objectweb.asm.Opcodes.INVOKESTATIC;
import static org.objectweb.asm.Opcodes.INVOKEVIRTUAL;
import static org.objectweb.asm.Opcodes.SIPUSH;

public class TreeAPIDemo {
    public static void printAllInsns(String classname) {
        InputStream in = HelloASM.class.getResourceAsStream(classname.replace('.', '/')+".class");

        ClassReader cr= null;
        try {
            cr = new ClassReader(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ClassNode classNode=new ClassNode();

        //ClassNode is a ClassVisitor
        cr.accept(classNode, 0);

        for(Object methodobj: classNode.methods){
            MethodNode methodNode = (MethodNode)methodobj;
            if (!methodNode.name.equals("equals")) continue;
            out.println("Instructions for method " + getMethodSig(methodNode));

            for (AbstractInsnNode insn: methodNode.instructions.toArray()) {
                String operands = "";
                if (insn instanceof VarInsnNode) {
                    operands += ((VarInsnNode)insn).var;
                }
                else if (insn instanceof JumpInsnNode) {
                    operands += ((JumpInsnNode)insn).label;
                }

                if (insn.getOpcode() > 0) {
                    out.println(insn + " " + Utils.opcodeToString(insn.getOpcode()) + " " + operands);
                    out.println("\t" + Utils.insnToString(insn));
                }
                else {
                    out.println(insn + " " + insn.getOpcode());
                }
            }
        }
    }

    public static void insertBlockID(String classname) {
        //InputStream in = HelloASM.class.getResourceAsStream(classname.replace('.', '/')+".class");
        InputStream in = null;
        try {
            in = new FileInputStream(classname+".class");
        }
        catch (Exception e) {

        }
        assert in != null;

        ClassReader cr = null;
        try {
            cr = new ClassReader(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ClassNode classNode=new ClassNode();

        //ClassNode is a ClassVisitor
        cr.accept(classNode, 0);

        int blockCount = 0;
        for(Object methodobj: classNode.methods){
            MethodNode methodNode = (MethodNode)methodobj;
            out.println("\nInstructions for method " + getMethodSig(methodNode));

            InsnList insnList = methodNode.instructions;
            List<AbstractInsnNode> points = new ArrayList<>();
            boolean foundMemAccess = false;

            AbstractInsnNode cur = insnList.getLast();
            while (cur != insnList.getFirst()) {
                if (Utils.containsMemAccess(cur)) {
                    foundMemAccess = true;
                }

                if (cur instanceof LabelNode) {
                    if (foundMemAccess) {
                        points.add(cur);
                        foundMemAccess = false;
                    }
                }
                cur = cur.getPrevious();
            }


            for (AbstractInsnNode p: points) {
                insnList.insert(p, getBlockIDCall());
                insnList.insert(p, getSipush(blockCount++));
            }
        }

        ClassWriter out = new ClassWriter(cr, ClassWriter.COMPUTE_FRAMES+ClassWriter.COMPUTE_MAXS);
        classNode.accept(out);
        ASMUtil.writeBytes("asmTreeDump/"+classname+".class", out.toByteArray());
        //output("asmTreeDump/"+classname+".class", out.toByteArray());
    }

    static void printMethodInsts(MethodNode method) {
        for (AbstractInsnNode insn: method.instructions.toArray()) {
            out.println(insn + " " + insn.getOpcode());
        }
    }

    static AbstractInsnNode getSipush(int i) {
        return new IntInsnNode(SIPUSH, i);
    }

    static AbstractInsnNode getBlockIDCall() {
        return new MethodInsnNode(INVOKESTATIC,"OHAMemCount", "blockID", "(S)V", false);
    }

    public static String getMethodSig(MethodNode m) {
        return m.name + m.desc;
    }


}
